package uz.pdp.codingbat.payload;

import lombok.Getter;

@Getter
public class AddLanguageDTO {

    private String title;
}
